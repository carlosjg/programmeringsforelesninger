# Matplotlib er en modul som kan tegne grafer/plots for oss

import matplotlib.pyplot as plt


# Vi trenger X-verdier (optional) og Y-verdier
X = [0, 1, 2, 3, 4, 5]
Y = [0, 1, 4, 9, 16, 25]

# Vi tegner verdiene med plot-funksjonen.
# Den kan ta i mot f.eks. '.', så det blir dotter.
# Dette kan du også droppe, for å få linjer.
plt.plot(X, Y, '.')

# Denne trengs ikke i f.eks. Jupyter Notebook, men trengs i Thonny.
plt.show()
