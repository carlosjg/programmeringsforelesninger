import numpy as np
import matplotlib.pyplot as plt


# Lager en smoothere plot med np.arange
X = np.arange(0, 6, 0.1)
Y = X**2

# Tegne grafen.
plt.plot(X, Y, '.')
plt.show()
