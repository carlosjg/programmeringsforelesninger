# Dictionaries består av key/indeks/felt og value/verdi, 
# og lager en slags tabellstruktur.
selskap = {'navn': 'Equinor', 'inntekt': 569000}

# Vi kan hente ut enkeltverdier ved å bruke key som indeks.
print(selskap['inntekt'])

# Vi kan oppdatere enkeltverdier på samme måte.
selskap['inntekt'] += 100000
print(selskap['inntekt'])

# Vi kan lage nye key-value-par/felt, ved å sette en ny key lik en verdi.
selskap['ansatte'] = 5000
print(selskap)
