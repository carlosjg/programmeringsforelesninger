# Dette kurset er et introduksjonsfag, og derfor må noen nyttige verktøy
# ekskluderes fra pensum. Pandas er et av disse. Veldig nyttig til behandling
# av f.eks. datasett, databaser, Excel-ark, osv. Her skal vi løse oppgaver
# hvor vi skal laste inn en csv-fil og lage en dictionary fra de, men vi gjør
# det med Pandas.
# For å kjøre koden trenger du:
# * Installer pandas
# * Sørg for at fil-pathen stemmer: Legg revenue2020.txt i en "data"-mappe


import pandas as pd


inntekts_dict_1 = pd.read_csv('data/revenue2020.txt', header=None, names=['Selskal', 'Inntekt']).to_dict('records')
print(inntekts_dict_1)

df = pd.read_csv('data/revenue2020.txt', header=None, names=['Selskaper', 'Inntekt']).T
df.columns = df.iloc[0]
inntekts_dict_2 = df.drop(df.index[0]).to_dict('records')[0]
print(inntekts_dict_2)
