# Denne funksjonen skal motta en fisketabell (store) og en fisketype.
def fish_amount(store, fisketype):
    for fisk, antall in store:
        if fisk == fisketype:
            return antall
    return 0


# Denne funksjonen gjør det samme som den forrige, men for-loopen splitter
# ikke opp en rad til fisk og antall på samme linje som for-delen.
# Kanskje derfor litt enklere. :)
def fish_amount_alt(store, fisketype):
    for rad in store:
        fisk, antall = rad
        if fisk == fisketype:
            return antall
    return 0


store = [
    ['torsk', 200],
    ['sei', 100]
]
print(fish_amount(store, 'torsk'))
print(fish_amount(store, 'månefisk'))
