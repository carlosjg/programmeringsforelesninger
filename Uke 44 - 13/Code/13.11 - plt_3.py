import matplotlib.pyplot as plt


X = [0, 1, 2, 3, 4, 5]
Y = [0, 1, 4, 9, 16, 25]

# Bar-funksjonen lager et bar-chart (stolpediagram)
plt.bar(X, Y)
plt.show()
