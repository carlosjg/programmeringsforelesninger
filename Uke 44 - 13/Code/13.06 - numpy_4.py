# Ikke heeelt pensum:
# Her tester vi hastigheten på å gange en liste med tall med 2.
# Versjon uten og med numpy.

import timeit


# Liste
kode_liste = '''
ny_liste = []
for num in liste:
    ny_liste.append(num * 2)
'''

print('liste:', timeit.timeit(
    setup = 'liste = list(range(10_000))',
    stmt = kode_liste,
    number = 1_000
))

# Array
kode_array = '''
nytt_array = 2 * array
'''

print('numpy-array:', timeit.timeit(
    setup = 'import numpy as np\narray = np.array(list(range(10_000)))',
    stmt = kode_array,
    number = 1_000
))
