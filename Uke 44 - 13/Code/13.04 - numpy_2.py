# Uten numpy:

# Lage funksjonen
def f(x):
    return x**2


# Lage X-verdier, og bygge opp en liste med Y-verdier.
X = list(range(6))
Y = []
for x in X:
    y = f(x)
    Y.append(y)

# Printe
print(X)
print(Y)
