# For å bruke numpy, må vi installere det med pakke-manageren i Thonny,
# eller med pip (utenfor pip)
import numpy as np


# Gangen en liste med 2: Mer liste.
liste = [1, 2, 3]
liste = liste * 2
print(liste)


# Gange et numpy-array med 2: Doble verdier i "listen".
array = np.array([1, 2, 3])
array = array * 2
print(array)
