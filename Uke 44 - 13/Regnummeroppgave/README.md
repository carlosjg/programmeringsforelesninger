Oppgave 3a jeg løste i forelesningen finnes i to/tre varianter:

* v1 - Den vi finner i Jupyter Notebook-notatene.
* v2.1 - Den jeg tok på stream for Ålesund og Trondheim
* v2.2 - Den jeg løste IRL for Gjøvik

Med disse har jeg også laget flowchart-diagrammer til hver, så dere kan se hvordan strukturen i if-ene er litt forskjellig. Strukturen i forelesningene er ca. det samme, men koden er skrevet litt forskjellig. Verdt å se på forskjellen i kodene.
