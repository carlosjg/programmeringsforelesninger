def check_registration(regnummer):
    if not (3 <= len(regnummer) <= 5):
        return False
    if not regnummer[0].isalpha():
        return False
    if not regnummer[1].isnumeric():
        return False

    if len(regnummer) == 3 and regnummer[2].isalpha():
        return True
    if len(regnummer) == 5 and regnummer[2].isnumeric() and regnummer[3:].isalpha():
        return True
    if len(regnummer) == 4 and regnummer[2].isalnum() and regnummer[3].isalpha():
        return True
    return False
