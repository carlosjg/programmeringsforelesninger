import time


# Lagre start-tid
start = time.time()

# Gjør noe som tar litt tid
x = 0
for i in range(10_000_000):
    x += 1

# Lagre slutt-tid
slutt = time.time()

# Finn tidsforskjell på start og slutt
total_tid = slutt - start

print(f'Det tok {total_tid} sekunder.')
