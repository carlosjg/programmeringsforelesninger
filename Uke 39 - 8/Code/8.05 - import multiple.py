# Importere hele modulen 'math'.
import math

print(math.pow(4, 2))
print(math.sqrt(16))
print(math.pi)


# Importere flere spesifikke funksjoner fra samme modul.
from math import pow, sqrt, pi

print(pow(4, 2))
print(sqrt(16))
print(pi)
