# Importere hele modulen 'random'.
import random

print(random.randint(1, 6))


# Importere 'randint'-funksjonen fra 'random'.
from random import randint

print(randint(1, 6))


# Importere 'randint', og kallen den 'ri'.
from random import randint as ri

print(ri(1, 6))
