# Renters rente-eksempel med funksjoner
# K = K0 * (1 + R)**n

# Definere funksjonen
def rentersrente(K0, R, n):
    K = K0 * (1 + R)**n
    return K

# Lage variabler
K0 = 10_000
R = 0.05
n = 13

# Kalle/kjøre funksjonen (kan også skrive verdier direkte inn i funksjonskallet, i stedet for å bruke variabler)
K = rentersrente(K0, R, n)
# K = rentersrente(10_000, 0.05, 13)

# Skrive ut resultatet, som en setning, ved hjelp av f-strings.
print(f'Hvis du putter {K0} kroner i banken i {n} år med en rente på {100*R}%, får du {K} kroner.')
