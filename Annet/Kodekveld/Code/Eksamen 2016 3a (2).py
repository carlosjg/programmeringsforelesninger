def sek_på_banen(lag, bane, tid):
    sekunder = tid * 60
    sekunder_per_pers = sekunder / lag
    personer_på_siden = lag - bane
    avrundet = round(sekunder_per_pers * personer_på_siden)
    return avrundet

print(sek_på_banen(6, 4, 12))
