votes = {
    'Trump': 60_000_000,
    'Hillary': 67_000_000,
    'Ron Paul': 200_000,
    'Jill Stein': 150_000,
    'Jesus': 1_000
}

for k, v in votes.items():
    print(f'{k} fikk {v} stemmer.')
    votes[k] += 1
