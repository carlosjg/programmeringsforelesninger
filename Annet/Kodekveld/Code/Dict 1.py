selskap = {
    'Equinor': 120_000,
    'Yara': 123
}

selskap['NTNU'] = 50_000

if 'UiO' in selskap:
    print(selskap['UiO'])
else:
    print(0)

print(selskap.get('UiO', 0))
