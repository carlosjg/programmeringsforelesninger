# Forslag på øvinger jeg kan gå igjennom

## Øving 2:
* Bilettpriser og rabatter

## Øving 3:
* Hangman
* Gjett tallet

## Øving 4:
* Lokale variabler
* Globale variabler
* Arbeidsdager

## Øving 5:
* Lister og løkker
* Lotto
* Chattebot
