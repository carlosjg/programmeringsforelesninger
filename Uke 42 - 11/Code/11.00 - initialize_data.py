# Kjøre denne først, så skal du ikke mangle noen nødvendige filer.

import os


if not os.path.exists('data'):
    os.mkdir('data')

csv_data = '''navn, email, passord
Paul, paul.knutson@ntnu.no, passord123
Joe Biden, potus@whitehouse.gov, trump_is_dumb
Erna, erna@erna.erna, ernaernaerna123'''

with open('data/data.csv', 'w') as f:
    f.write(csv_data)
