# I vanlige plaintekst-filer, kan vi skrive og lese strings.
# Men da må vi gjøre om tall, lister, tabeller, dicts. osv.
# til strings først, og så skrive. For å lese igjen, må vi
# lese inn teksten, og så konvertere og konstruere tall,
# lister, osv. osv. Dette funker ofte greit, og det er
# fordeler med å bruke plaintekst-filer, men noen ganger
# hadde det vært mer praktisk å kunne lagre og lese
# variabler direkte, i stedet for å konvertere til/fra str.
# Det kan vi bruke pickle til! :D


import pickle


#################################
## Kopiert fra forrige oppgave ##
#################################

# Lese inn csv-filen.
with open('data/data.csv', 'r') as fil:
    tekst = fil.read()


# Konvertere csv-filen til en tabell.
def csv_til_tabell(tekst):
    # Splitte tekst inn i liste av rader, og fjern headers.
    rader = tekst.split('\n')
    rader = rader[1:]

    # Lag liste av rader til liste av lister.
    tabell = []
    for rad in rader:
        ny_rad = rad.split(', ')
        tabell.append(ny_rad)

    return tabell

tabell = csv_til_tabell(tekst)


###############
## Pickle :D ##
###############

# Konvertere tabellen til binærdata.
binary_data = pickle.dumps(tabell)

# Skrive binærdata til fil, i binary-write-modus.
with open('data/pickle_testdata', 'wb') as fil:
    fil.write(binary_data)

# Vi leser filen inn i neste program.
