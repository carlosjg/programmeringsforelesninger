## Oppgave:
# Du er på Facebook eller noe sånt, og noen har postet en sånn
# "Kari og Bjarne gikk inn i et rom. Der er det fire ender som sitter på stoler
#   rundt et bord, to griser som ligger på sengen, og en rotte som hopper i
#   fallskjerm. Hvor mange bein er det i rommet?"
# Orker vi å telle? Nei. Vi lager heller et program som gjetter igjen og igjen,
#   og så kan vi kopiere gjettene inn i kommentarfeltet. :D


# For alle tall fra 0 til (men ikke med) 101, skriv ut teksten: "Er det {i}?".
for i in range(101):
    print(f'Er det {i}?', end=' ')
