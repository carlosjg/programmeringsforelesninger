# Vi skriver inn x-verdiene vi vil finne y-verdier for,
# og så finner vi y-verdiene ved hjelp av funksjonen f(x):
# f(x) = x**2

# Definere funksjonen
def f(x):
    # return x**2
    y = x**2
    return y


# Vi lager en liste med x-verdier, og en tom liste med y-verdier.
x_verdier = [0, 1, 2, 3, 4, 5, 6]
y_verdier = []

# for en og en x-verdi (x), finn y=f(x), og legg til i listen med y-verdier.
for x in x_verdier:
    y = f(x)
    y_verdier.append(y)

print(y_verdier)
