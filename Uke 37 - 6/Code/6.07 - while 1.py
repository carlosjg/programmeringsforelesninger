## if vs while
# if of while har veldig lik syntaks:

# if <påstand>:
#     <gjør ting>

# while <påstand>:
#     <gjør ting

x = 0

# if
print('if:')
if x < 3:
    print(f'{x} < 10')
print()

# while
print('while:')
while x < 3:
    print(f'{x} < 10')
    x += 1

# Whilen trenger to endringer:
# 1. Bytt ut kodeordet "if" med "while"
# 2. Sørg for at påstanden vil bli False på et eller annet tidsspunkt.
#   Om vi glemmer "x += 1" i whilen, vil x alltid være 0, og whilen vil
#   fortsette for alltid. Da må vi avbryte programmet, ved å trykke på
#   stoppknappen.
