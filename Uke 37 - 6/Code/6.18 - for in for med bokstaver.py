# Lager en liste med navn.
navneliste = ['Paul', 'Ada', 'Obama', 'Alan']

# Går igjennom ett og ett navn i navnelisten.
for navn in navneliste:

    # Går igjennom en og en bokstav i navnet,
    #   og skriver det ut med mellomrom mellom hver bokstav.
    for bokstav in navn:
        print(bokstav, end=' ')

    # Tom print gir en ny linje, så hvert navn kommer på hver sin linje.
    print()
