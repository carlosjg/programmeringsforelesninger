x = 0


print('if:')
if x < 3:
    print(f'{x} < 10')
print()


print('while:')
while x < 3:
    print(f'{x} < 10')
    x += 1
