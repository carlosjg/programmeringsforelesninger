# Vi lager en liste med oddetallene under 10 (som vi fyller ut manuelt).
talliste = [1, 3, 5, 7, 9]
print(talliste)

# Her lager vi en liste over noen navn, og skriver de ut.
navneliste = ['Paul', 'Ada', 'Obama', 'Alan']
print(navneliste)
