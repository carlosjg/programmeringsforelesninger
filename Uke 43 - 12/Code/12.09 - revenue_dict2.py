file_path_2020 = 'data/revenue2020.txt'


# Denne funksjonen leser inn filen som heter/ligger i fpath.
def les_fil(fpath):
    with open(fpath, 'r') as f:
        tekst = f.read()
    return tekst


# Denne funksjonen tar i mot en string i csv-format, og lager en dict av den.
def csv_to_dict(csv_tekst):
    linjer = csv_tekst.split('\n')

    inntekts_dict = {}

    for linje in linjer:
        if len(linje) > 0:
            selskap, inntekt = linje.split(',')

            inntekts_dict[selskap] = int(inntekt)
    return inntekts_dict


csv_tekst = les_fil(file_path_2020)
inntekts_dict = csv_to_dict(csv_tekst)
print(inntekts_dict)
