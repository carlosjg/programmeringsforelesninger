# Oppgave fra øving

# Dette er ca. det vi vil ha.
# inntekts_dict = {
#     'Equinor': 566342,
#     'Norsk Hydro': 149766,
#     'Yara International': 113837,
# }
# print(inntekts_dict)


# Disse pathene er fra oppgaven, men jeg har endret de litt,
# fordi mine filer ligger i data-mappen.
file_path_2020 = 'data/revenue2020.txt'
file_path_2021 = 'data/revenue2021.txt'


# Denne funksjonen leser inn filen som heter/ligger i fpath.
def les_fil(fpath):
    with open(fpath, 'r') as f:
        tekst = f.read()
    return tekst


# Denne funksjonen tar i mot en string i csv-format, og lager en dict av den.
def csv_to_dict(csv_tekst):
    # Splitter teksten i linjer.
    linjer = csv_tekst.split('\n')
    
    # Lage en tom dict, som vi senere kan fylle med verdier.
    inntekts_dict = {}

    # Gå gjennom en og en linje.
    for linje in linjer:
        # Sjekke at linjen faktisk har verdier, ved å se om
        # lengden er større enn 0.
        if len(linje) > 0:
            # Vi kan splitte linjer på denne måten.
            # rad = linje.split(',')
            # selskap = rad[0]
            # inntekt = rad[1]

            # Eller denne måten. :D
            selskap, inntekt = linje.split(',')

            # Legge til selskapet og inntekten (som heltall) i dicten.
            inntekts_dict[selskap] = int(inntekt)
    return inntekts_dict


# Her bruker vi funksjonene vi har laget, og
# skriver ut den resulterende dicten.
csv_tekst = les_fil(file_path_2020)
inntekts_dict = csv_to_dict(csv_tekst)
print(inntekts_dict)

# print(inntekts_dict['KLP'])
