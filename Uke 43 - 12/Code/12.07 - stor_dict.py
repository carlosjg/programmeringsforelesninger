# Stor dict :D
# Sånn kan vi lagre store mengde variert data.
bruker = {
    'navn': 'Arne',
    'alder': 45,
    'bil': {
        'merke': 'BMW',
        'årstall': 2012
    },
    'har hest': True,
    'barn': ['Per', 'Pål', 'Erna Askeladd']
}

print(bruker)

# Vi kan aksessere elementer på litt samme måte som i tabeller,
# bare med våre egne indekser.
print(bruker['bil']['merke'])
