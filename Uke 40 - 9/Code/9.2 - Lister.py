# Lag liste med navn
navneliste = ['Paul', 'Karen', 'Bush']
print(1, navneliste)

# Legge til element i listen
navneliste.append('Erna')
print(2, navneliste)

# Oppdatere et element i listen
navneliste[1] = 'Obama'
print(3, navneliste)

# Fjerne (og returnere)
navn = navneliste.pop()
print(4, navneliste, f'Tok vekk {navn}')

# Fjerne element i listen (by index)
del navneliste[2]
print(5, navneliste)

# Fjerne element i listen (by value)
navneliste.remove('Paul')
print(6, navneliste)

# Doble listen
navneliste = navneliste * 2
print(7, navneliste)

# Plusse sammen lister
navneliste += ['Arne', 'Putin', 'Jens', 'Gro']
print(8, navneliste)

# Reversere rekkefølge
navneliste.reverse()
print(9, navneliste)

# Sortere liste
navneliste.sort()
print(10, navneliste)

# Sortere i motsatt rekkefølge
navneliste.sort(reverse=True)
print(11, navneliste)

# Alternativ måte å sortere navnelisten
navneliste = sorted(navneliste)
print(navneliste)

# Finne lengde på liste
print(len(navneliste))

# Telle antall verdier lik et eller annet
print(navneliste.count('Obama'))

# Finne indeksen til en verdi
print(navneliste.index('Jens'))

# Sjekke om element finnes i listen
print('Gro' in navneliste)
