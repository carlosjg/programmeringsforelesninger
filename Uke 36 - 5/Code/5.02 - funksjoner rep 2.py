### Eksempel på skrive-til-fil-funksjon.
def skriv_til_fil(filnavn, tekst):
    # gjorde vi ting her.
    # ("pass" betyr "gjør ingenting".
    #   Ellers blir Python sur fordi vi mangler kode inni funksjoen.)
    pass
    

###  Eksempel på terningkast-funksjon

# Importere nødvendig modul (dette gjøres gjerne i toppen av filen)
import random

# Definere funksjonen
def kast_terning():
    kast = random.randint(1, 6)
    return kast

# Returverdien fra funksjonen kan mellomlagres og så skrives ut ...
kast1 = kast_terning()
print(kast1)

# ... eller skrives direkte ut, uten mellomlagring.
print(kast_terning())
