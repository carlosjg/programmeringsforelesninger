## Programmet skal finne antall dager i februar, basert på årstallet.
# Funksjonen er_skuddår tar i årstall, og returnerer
#   True hvis der er skuddår, og False hvis ikke.
# Funksjonen antall_dager_i_februar tar i mot årstall,
#   og returnerer 29 hvis det er skuddår, og 28 hvis ikke.
# antall_dager_i_februar bruker funksjoen er_skuddår til å vurdere
#   om det er skuddår eller ikke.
# Dermed har vi en funksjon som bruker en annen funksjon inni seg.

def er_skuddår(år):
    # Hvis årstallet er delelig på 4, er det skuddår*.
    # * Reglene er mer kompliserte, men vi forenkler litt.
    if år % 4 == 0:
        return True
    else:
        return False


def antall_dager_i_februar(år):
    # Kall funksjonen "er_skuddår" for å sjekke om det er skuddår.
    if er_skuddår(år):
        return 29
    else:
        return 28


print(antall_dager_i_februar(2020))
print(antall_dager_i_februar(2022))


#########################################
## Alternative varianter av koden over ##
#########################################

## Alternativ variant er_skuddår
# Denne er mye kortere, men litt mer komplisert.
# I originalfunksjonen:
#   * Hvis "år%4 == 0" er True, skal vi returnere True
#   * Hvis "år%4 == 0" er False, skal vi returnere False
# Altså skal vi bare returnere det samme som vi får av "år%4 == 0".
# Dermed kan vi bare returnere boolen vi får fra "år%4 == 0".
# Dette er mer effektivt, men krever kanskje en litt bedre forståelse av bools.
def er_skuddår(år):
    return år % 4 == 0


## Alternativ variant av antall_dager_i_februar-funksjonen
# Her mellomlagres den boolske verdien vi får fra er_skuddår, før vi bruker den.
def antall_dager_i_februar_2(år):
    # Kall funksjonen "er_skuddår" for å sjekke om det er skuddår.
    skuddår = er_skuddår(år)
    if skuddår:
        return 29
    else:
        return 28


## Alternativ variant av hele koden, uten funksjoner.
# Kortere og kanskje enklere, men litt mindre praktisk senere i kurset.
# Nå kan du ikke spørre om det er skuddår andre steder i koden, uten å kopiere kode.
år = 2022

if år % 4 == 0:
    dager_i_feb = 29
else:
    dager_i_feb = 28

print(dager_i_feb)
