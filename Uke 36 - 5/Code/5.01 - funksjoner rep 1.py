### Matematisk funksjon

# Definere funksjonen
def f(x):
    y = x**2
    return y

# Bruke funksjonen
y0 = f(0)
y3 = f(3)

# Skrive ut resultatene
print(y0, y3)
