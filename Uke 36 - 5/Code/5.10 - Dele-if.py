## Sikker deling
# Hvis vi deler a på b, så kan vi gjøre det slik:
#   a / b
# Men hvis b er null, så krasjer programmet.
# Vi kan legge inn en sjekk, som ser om b == 0.
# Om b == 0, så kan vi gjør noe annet.
# I dette tilfellet, returnerer vi bare et stort tall, siden
#   x/0 på en måte blir uendelig stort.
#   (Egentlig er det jo ikke det, men jeg er ikke mattelæreren din :)

def dele(a, b):
    if b == 0:
        resultat = 10000000000
    else:
        resultat = a / b
    return resultat


print(dele(10, 0))
