# Bruker fyller inn alderen sin
alder = 19

## Programmet sjekker hva slags alkohol, om noe, brukeren har lov til å kjøpe

# Hvis alder er mindre enn 18, kan du ikke kjøpe.
if alder < 18:
    print('Kan ikke kjøpe alkohol.')

# Ellers: Hvis alder er mindre enn 20 (men ikke mindre enn 18), kan du kjøpe øl.
elif alder < 20:
    print('Kan kjøpe øl.')

# Hvis ingen av de forrige testene er sanne, kjøres denne.
# Da er du 20 eller eldre, og kan kjøpe hva du vil. :D
else:
    print('Kan kjøpe øl og sprit.')
