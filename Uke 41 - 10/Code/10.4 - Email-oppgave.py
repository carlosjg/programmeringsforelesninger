# Oppgave: Lag en funksjon som tar variablen 'adresser' og returnerer
# forventet resultat (under).
adresser = 'abc@gmail.com;def@hotmail.no;ghi@ntnu.no;jkl@outlook.com'

# Forventet resultat:
'''
abc@gmail.com
def@hotmail.no
ghi@ntnu.no
jkl@outlook.com
'''

# Løsning: Vi ser at forskjellen er at semikolon (;) er
# byttet ut med linjeskift (\n). Her kan vi bruke str.replace(a, b)


# Funksjonen bytter ur ';' med '\n', og returnerer.
def fiks_mailadresser(adresser):
    adresser = adresser.replace(';', '\n')
    return adresser


print(fiks_mailadresser(adresser))
