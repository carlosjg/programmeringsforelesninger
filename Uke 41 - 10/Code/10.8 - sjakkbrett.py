# Denne rakk vi ikke helt, men her viser jeg en måte en kan sette opp et
# sjakkbrett. Det er ikke sjakk enda. Ingen ordentlig funksjonalitet her.
# Men det viser hvordan en kan "lagre" et sjakkbrett. Jeg har også lagt til en
# funksjon som printer ut sjakkbrettet pent-ish, og flyttet på en bonde/pawn.
# Dette kan bygges videre på. :D
# But also, du kan bruke den eksterne modulen 'chess', og så gjør den alt for
# deg. Inkludert å sjekke om trekkene er lovlige. :DD

chessboard = [
    ['r', 'h', 'b', 'k', 'q', 'b', 'h', 'r'],
    ['p', 'p', 'p', 'p', 'p', 'p', 'p', 'p'],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    ['P', 'P', 'P', 'P', 'P', 'P', 'P', 'P'],
    ['R', 'H', 'B', 'K', 'Q', 'B', 'H', 'R']
]


# Mye av det samme som print_table. Se om du kan tolke hva som skjer og hva som er forskjellig! :D
def print_chessboard(table):
    # La på litt ekstra for å lage firkanten rundt brettet
    print(' ' + '-'*24)
    for row in table:
        print('|', end='')
        for element in row:
            print(f' {element} ', end='')
        print('|')
    print(' ' + '-'*24)


print_chessboard(chessboard)

chessboard[4][0] = chessboard[6][0]
chessboard[6][0] = ' '

print_chessboard(chessboard)
