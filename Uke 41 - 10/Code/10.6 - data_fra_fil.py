# Oppgave: Vi har en tekst (som vi later som kom fra en fil)
# Den er i CSV-format. Altså strukturert som i variabler 'tekst'.
# Vi skal lage tabellen om til en Python-tabell (altså 2D-lister).

tekst = '''navn, email, passord
Paul, paul.knutson@ntnu.no, passord123
Joe Biden, potus@whitehouse.gov, trump_is_dumb
Erna, erna@erna.erna, ernaernaerna123'''

'''
[
    ['Paul', 'paul.knutson@ntnu.no', 'passord123'],
    [...],
    ...
]
'''

# Først splitter vi teksten inn i en liste med separate linjer.
linjer = tekst.split('\n')

# Så fjerner vi første linje (titlene). Her er tre måter å gjøre det på.
# del linjer[0]
# linjer.pop(0)
linjer = linjer[1:]

# Vi lager en tom tabell (liste)
tabell = []

# Vi går igjennom en og en linje
for linje in linjer:
    # Splitter opp elementene til en rad (liste) med elementer
    rad = linje.split(', ')

    # Og legger til hver rad (liste) i tabellen (liste).
    tabell.append(rad)


# Denne kopierte jeg fra forrige uke. Den skriver ut tabeller.
# Eneste endring: Kolonnebredde.
def print_table(table):
    for row in table:
        for element in row:
            print(f'{element:25}', end=' ')
        print()

print_table(tabell)
