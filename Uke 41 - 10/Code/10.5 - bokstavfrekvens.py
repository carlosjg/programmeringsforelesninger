# Oppgave: Skriv ut antallet ganger en bokstav gjentar seg i en tekst.
# Eksempel:
'Hei sann'
# skal bli til
'''
a: 1
e: 1
h: 1
i: 1
n: 2
s: 1
'''


tekst = 'Her er noe tekst som kan brukes som eksempel. Håper det holder fint for det vi skal prøve på nå.'


def bokstavfrekvens(tekst):
    alfabet = 'abcdefghijklmnopqrstuvwxyzæøå'

    # Tving teksten til å være lowercase.
    tekst = tekst.lower()
    
    # Gå gjennom alfabetet, en og en bokstav.
    for bokstav in alfabet:
        # Tell antallet ganger bokstaven gjentar seg i teksten.
        antall = tekst.count(bokstav)

        # Hvis den finnes mer enn 0 ganger (som vil si at den finnes i teskten),
        if antall > 0:
            # Skriv ut bokstaven og antallet.
            print(f'{bokstav}: {antall}')


# bokstavfrekvens('hei sann')
bokstavfrekvens(tekst)

# Resultatet av dette kan alltids lagres i to lister (bokstav-liste og antall-liste)
# og plottes med matplotlib.pyplot (plt.bar(bokstav_liste, antall_liste))
