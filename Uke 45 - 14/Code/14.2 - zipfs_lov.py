'''
fil -> frekvens-plot

1. Les inn tekst fra fil
2. Splitt tekst til en ordliste
3. Telle antallet av hvert ord (i en dict)
4. Sortere verdiene etter antallet ord
5. Plot verdiene/listen(e) (eller de 10 første, hvertfall)

Vi skriver inn eksempeldata før hver funksjon,
så vi kan se hva de gjør.
Eksempelfilen inneholder teksten:
'1:1 Er det det det er?'
'''


import matplotlib.pyplot as plt


# eksempelfil.txt -> '1:1 Er det det det er?'
def les_fra_fil():
    with open('bible.txt', 'r') as f:
        tekst = f.read()
    return tekst


# '1:1 Er det det det er?' -> '    er det det det er '
def clean_data(tekst):
    tekst = tekst.lower()
    for tegn in '/\\@.,-:!?0123456789':
        tekst = tekst.replace(tegn, ' ')
    return tekst


# '    er det det det er ' -> ['er', 'det', 'det', 'det', 'er']
def lag_ordliste(tekst):
    return tekst.split()


# ['er', 'det', 'det', 'det', 'er'] -> {'er': 2, 'det': 3}
def tell_antall_ord_1(ordliste):
    ordfrekvenser = {}
    for Ord in ordliste:
        #if Ord in ordfrekvenser:
        #    ordfrekvenser[Ord] += 1
        #else:
        #    ordfrekvenser[Ord] = 1
        
        if Ord not in ordfrekvenser:
            ordfrekvenser[Ord] = 0
        
        ordfrekvenser[Ord] += 1
    return ordfrekvenser


# ['er', 'det', 'det', 'det', 'er'] -> {'er': 2, 'det': 3}
def tell_antall_ord_2(ordliste):
    # Treg. Ikke bruk.
    ordfrekvenser = {}
    ordsett = set(ordliste)
    for Ord in ordsett:
        ordfrekvenser[Ord] = ordliste.count(Ord)
    return ordfrekvenser


# {'er': 2, 'det': 3} -> [(3, 'det'), (2, 'er')]
def sorter_ordfrekvenser(ordfrekvenser):
    ordfrekvensliste = []
    
    #for par in ordfrekvenser.items():
    #    Ord, frekvens = par
    for Ord, frekvens in ordfrekvenser.items():
        par = (frekvens, Ord)
        ordfrekvensliste.append(par)
    ordfrekvensliste.sort(reverse=True)
    return ordfrekvensliste


# [(3, 'det'), (2, 'er')] -> ['det', 'er'], [3, 2]
def lag_til_lister(ordfrekvensliste):
    tittelliste = []
    frekvensliste = []
    
    for frekvens, Ord in ordfrekvensliste:
        tittelliste.append(Ord)
        frekvensliste.append(frekvens)
    return tittelliste, frekvensliste


# Bruk alle funksjonene.
tekst = les_fra_fil()
tekst = clean_data(tekst)
ordliste = lag_ordliste(tekst)
ordfrekvenser = tell_antall_ord_1(ordliste)
ordfrekvensliste = sorter_ordfrekvenser(ordfrekvenser)
tittelliste, frekvensliste = lag_til_lister(ordfrekvensliste)

# Plott verdiene våre.
# Google: Zipf's law for mer om akkurat dette.
plt.plot(frekvensliste)
plt.xscale('log')
plt.yscale('log')
plt.savefig('Zipfs_lov.png')
plt.show()
