# Renters rente beregnes på følgene måte:
# K = K0 * (1 + R)**n

# Variabler brukeren kan fylle inn
startsum = 150_000
rente = 0.05
år = 13

# Beregning av K
sluttsum = startsum * (1 + rente)**år

# Printe resultat
print(sluttsum)
