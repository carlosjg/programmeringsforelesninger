# Programmeringsforelesninger

Hei! Her ligger kode, notater, slides/PowerPoint og PDFer fra forelesningene, eller hvertfall noe av det.
De første heter Uke __A og Uke __B, hvor A er enkelttimene vi vanligvis har teori i (mandag) og B er dobbelttimene (tirsdag/onsdag/fredag).
I hver ukemappe ligger det en eller flere av følgende:

* en Jupyter-fil (.ipynb) med forelesningsnotater. Dette kan brukes med Jupyter Notebook i nettleseren (se guide lenger nede).
* en mappe ("Code") som har koden jeg skrev i forelesningen. (Det er ofte fra tirsdagstimene, så hvis du er på Gjøvik-forelesningene kan det hende at det er noen små forskjeller.)
* en PDF med PowerPoint/Menti-presentasjonen, der jeg brukte det.

Og kanskje noe mer, som ikke står her.


## Forelesningsplan

Planen for TDT4109/TDT4110 (et av parallellfagene vårt fag er basert på) hadde sånn ca. følgende forelesningsplan i 2021, og vi kommer nok til å basere oss en del på den. Det kan dermed hende vi ikke følger den helt til punkt å prikke, men det blir nok ikke store forskjeller derfra.

34. Fag- og programmingsintroduksjon
35. Variabler, datatyper, bruk av funksjoner
36. Betingelser og logiske uttrykk
37. Løkker (for og while)
38. Funksjoner
39. Mer om funksjoner, moduler
40. Mer om iterables (lister o.l.)
41. Mer om strings
42. Filer og exceptions
43. Dict og set
44. Repetisjon (1) + ekstramoduler
45. Repetisjon (2)
46. Repetisjon (3)
47. Oppsummering/spørretime

Et par forskjeller:

* Vi begynte med variabler og datatyper allerede i uke 34.
* Planen er å begynne å introdusere funksjoner litt tidligere.


## Mer detaljert forelesningsplan

Her er en mer detaljert forelesningsplan i tabellform. For noen kommer ikke tabellen opp. Da kan du sjekke Annet-mappen (hvis den finnes enda) for et screenshot.

|Uke|Innhold|Mer om innholdet|
|---|---|---|
|34|Fag- og programmeringsintroduksjon|* Generell introduksjon til faget <br>* Installasjon av Thonny <br>* Bruk av CLI og editor|
|35|Variabler, datatyper og bruk av funksjoner|* Variabler <br> * Datatyper og konvertering <br> * Bruk av innebygde funksjoner <br> * Introduksjon til funksjoner|
|36|Betingelser og logiske uttrykk|* bool <br> * if, elif og else|
|37|Løkker|* while <br> * for (range og lister) <br> * Lister|
|38|Funksjoner|* Definisjon/konstruksjon av funksjoner <br> * Input-parametere <br> * Returnering av tuples <br> * Namespace|
|39|Moduler (og mer funksjoner)|* Innebyggede <br> * Installasjon av eksterne moduler i Thonny <br> * Kort om pip|
|40|Mer om iterables|* Lister, tuples og strings <br> * Iterering over itererbare objekter (for) <br> * Mer om indeks <br> * Innbyggede funksjoner (sort, reverse) og len|
|41|Mer om strings|* Innebyggede funksjoner <br> * Iterering (?)|
|42|Filer og exceptions|* Metoder å åpne filer <br> * Lese-/skrivemodus <br> * Generell except <br> * Spesifikke except|
|43|Dict og set|* Intro til begge <br> * Dict-funksjoner og indeksering <br> * Sett i matematikken og i kode|
|44|Repetisjon (1) + ekstramoduler|* Repetisjon <br> * NumPy <br> * Matplotlib|
|45|Repetisjon (2)|* Repetisjon|
|46|Repetisjon (3)|* Repetisjon|
|47|Oppsummerings-/spørretime|* Spørsmål og sånt|


## Åpne Jypyter-filer i GitLab/nettleseren

* Bla gjennom Git-repoet og åpne en fil som slutter på ".ipynb".
* Over filen er det en blå knapp hvor det står "Open in Web IDE", "Edit" eller "Open in Gitpod".
* Åpne undermenyen på denne knappen, og velg "Open in Gitpod.
* Trykk på "hoveddelen" av knappen.
* Da åpner et Gitpod dashboard seg, hvor du må trykke på den grå knappen.
* Så åpner VS Code seg i nettleseren. Du har filstrukturen til repoet på venstre side.

Nå skal du kunne kjøre filer eller celler i filene.
(Det kan hende du må lukke og åpne filen på nytt, hvis den ser teit ut.)
