# Vi vil lage en funksjon som skriver ut en tekst fancy.
# Hvis vi gir funksjonen 'fancy_print' teksten 'hei sann', skal den skrive ut:

############
# hei sann #
############


def fancy_print(tekst):
    # Ting vi ikke har gått igjennom helt enda: len()
    # len(tekst) eller len(liste) gir tilbake lengden av teksten/listen.
    antall = len(tekst) + 4
    print('#' * antall)
    print('#', tekst, '#')
    print('#' * antall)


fancy_print('hei sann')
