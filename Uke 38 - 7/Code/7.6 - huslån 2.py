# maks: 5 * årslønn
# egenkapital: 0.15 * huslån

def maks_huslån(årslønn, egenkapital, annet_lån=0, kredittkortgrense=0):
    maks_fra_årslønn = årslønn * 5
    maks_fra_egenkap = egenkapital / 0.15
    maksialt_totalt_lån = min(maks_fra_årslønn, maks_fra_egenkap)
    maksialt_huslån = maksialt_totalt_lån - annet_lån - kredittkortgrense
    return maksialt_huslån


print(maks_huslån(200_000, 150_000, 100_000, 50_000))
print(maks_huslån(200_000, 150_000))


# Dette er litt som range: Den kan ta i mot 1, 2 eller 3 verdier.
# Vår funksjon nå kan ta i mot 2, 3 eller 4 verdier.
# range(10) -> 0, 1, ..., 9
# range(3, 8) -> 3, 4, 5, 6, 7
# range(3, 8, 2) -> 3, 5, 7
