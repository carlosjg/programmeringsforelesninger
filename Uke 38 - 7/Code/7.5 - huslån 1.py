# Regler for huslån du kan ta opp.
# maks: 5 * årslønn
# egenkapital: 0.15 * huslån (15%)

# Vi skal lage to funksjoner:
# Funksjon 1: maks_huslån(årslønn, egenkapital) -> huslån
# Funksjon 2: huslånkrav(huslån) -> (minste_årslønn, egenkapital)


def maks_huslån(årslønn, egenkapital):
    maks_fra_årslønn = årslønn * 5
    maks_fra_egenkap = egenkapital / 0.15
    maksialt_huslån = min(maks_fra_årslønn, maks_fra_egenkap)
    return maksialt_huslån


print(maks_huslån(500_000, 300_000))


def huslånkrav(huslån):
    minste_årslønn = huslån / 5
    egenkapital = 0.15 * huslån
    return (minste_årslønn, egenkapital)


print(huslånkrav(2_000_000))

# huslånkrav returnerer 2 verdier. a, b er 2 verdier. Derfor kan vi også skrive
# a, b = huslånkrav(2_000_000)
# Det er ikke ofte vi trenger det, og jeg tror ikke det er pensum, men noen
# moduler og noen på nettet vil absolutt, og da er det greit å ha sett det før.
