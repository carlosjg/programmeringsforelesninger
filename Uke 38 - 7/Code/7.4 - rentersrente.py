# Denne har vi sett før

# K = K0 * (1 + R)^n


K0 = 10_000
R = 0.05
n = 13


# Gir vi argumentene/variablene tydelige navn,
# er det lett å skjønne hva som skjer.
# Da blir det mindre nødvendig med kommentarer.
def beregn_K(startsum, rente, perioder):
    sluttsum = startsum * (1 + rente)**perioder
    return sluttsum


print(beregn_K(K0, R, n))
