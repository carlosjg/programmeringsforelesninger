# f(x) = x^2

# Uten funksjon
x = 3
y = x**2
print(y)


# Med funksjon
def f(x):
    return x**2


x = 3
y = f(x)
print(y)
